import './App.css';
import avatar from './assets/images/48.jpg'
function App() {
  return (
    <div className='devcamp'>

        <img className='dc-avatar' src={avatar} alt=" tamy stevens "/>
      
      <div className='dc-puote'>
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>

      </div >
      <p className='dc-name'>
          tamy stevens
          <samp>
          * Front End Developer
          </samp>
      </p>
    </div>
  );
}

export default App;
